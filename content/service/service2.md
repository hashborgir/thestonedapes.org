+++
title = "Psychedelic Events"
+++

We are building a global community of psychedelic authors, thinkers, poets, speakers, artists, and musicians. To that end we hold psychedelic live music, open mic, poetry and educational events for the community.