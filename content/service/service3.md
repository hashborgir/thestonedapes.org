+++
title = "Psychedelic Coaching"
+++

Psychedelic coach training. The Stoned Apes Certified Psychedelic Coach would know the essentials of trip sitting, harm reduction, psychological safety and to ensure safe and beautifully enlightening journeys.