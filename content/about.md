+++
menu = "main"
title = "About"
type = "about"
weight = 10
+++

# Who Are The Stoned Apes

The Stoned Apes are a non-profit organization dedicatd to spreading awareness about responsible psychedelic use. The Stoned Ape theory was first proposed by Dr. Terence McKenna.

The Stoned Apes are here to raise awareness about psychedelics and to alleviate the stigma associated with their use.


# Our Humble Beginnings
### Aug 2010
The Stoned Apes started as a personal project to cater to the many growing needs of the Psychedelic Community. It has since grown to include many beautiful souls and has incubated many projects.

# Birth of a Non Profit
### Mar 2011
The Stoned Apes started hosting meetups to start spreading awareness. We are now proud to host many events and meetups every month.
￼
# Stoned Apes Community Council
### Jul 2016
The Stoned Apes Community council was created in order to oversee the continuing growth of the psychedelic community.